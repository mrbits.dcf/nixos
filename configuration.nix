{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

# You must change this to adapt your own HD configuration
  boot.initrd.luks.devices = [
    {
      name = "root";
      device = "/dev/sda3";
      preLVM = true;
    }
  ];

  boot.loader.grub.device = "/dev/sda";

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "Nixon"; # Define your hostname.
  #networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;  # Enables wireless support via wpa_supplicant.

  # Select internationalisation properties.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "br-abnt2";
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "America/Sao_Paulo";

  # Users can use zsh

  # Configure user
  users.extraUsers.mrbits = {
    name = "mrbits";
    group = "users";
    extraGroups = ["wheel" "disk" "audio" "video" "networkmanager" "system-jornald" "docker" "vboxusers"];
    createHome = true;
    uid = 1000;
    home = "/home/mrbits";
    shell = "/run/current-system/sw/bin/zsh";
  };

  security = {
    sudo = {
      wheelNeedsPassword = false;
    };
  };

  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    vim
    wget
    curl
    openssl
    git
    zip
    unzip
    bash-completion
    fish
    jq
    lsof
    tmux
    tree
    which
    nmap
    awscli
    packer
    terraform
    vagrant
    virtualbox
    stdenv
    gcc
    gnumake
    automake
    autoconf
    python27Full
    python27Packages.ipython
    python3
    python27Packages.virtualenvwrapper
    zsh
    oh-my-zsh
    xorg.xkill
    firefox
    hexchat
    transmission_gtk
    gparted
    docker
    python27Packages.docker_compose
    skype
    pidgin
    pidginotr
    slack
    libreoffice-still
    openvpn
    gnome3.gnome-tweak-tool
    gnome3.networkmanager_openvpn
  ];

  # Configure programs
  programs.zsh.enable = true;
  programs.zsh.interactiveShellInit = ''
    export ZSH=${pkgs.oh-my-zsh}/share/oh-my-zsh/

    ZSH_THEME="pygmalion"
    plugins=(git virtualenv docker docker-compose zsh-completions vagrant)
    source $ZSH/oh-my-zsh.sh
  '';
  programs.zsh.promptInit = "";

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  #services.xserver.synaptics.enable = true;
  services.xserver.layout = "br";
  services.xserver.xkbVariant = "abnt2";
  services.xserver.xkbOptions = "eurosign:e";

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  # Enable XMonad
  services.xserver.windowManager.xmonad.enable = true;
  services.xserver.windowManager.xmonad.enableContribAndExtras = true;

  # Enable GNOME 3 Desktop Environment.
  services.xserver.displayManager.lightdm.enable = true;
  services.xserver.desktopManager.gnome3.enable = true;

  services.xserver.multitouch.enable = true;

  virtualisation = {
    virtualbox.host = {
      enable = true;
      enableHardening = false;
      addNetworkInterface = true;
    };
    docker = {
      enable = true;
    };
  };

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "17.03";

}
